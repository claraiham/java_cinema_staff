public class App {
    public static void main(String[] args) throws Exception {
        String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
        int[] hoursWorked = {35, 38, 35, 38, 40};
        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};
        String searchPosition = "Caissier";

        for(int i= 0; i < employeeNames.length; ++i){
            if(hoursWorked[i] > 35){
                int overhours = hoursWorked[i] - 3;
                System.out.println( employeeNames[i]+ " " + (35* hourlyRates[i] + overhours*hourlyRates[i]*1.5));
            } else {
                System.out.println( employeeNames[i]+ " " + hoursWorked[i]* hourlyRates[i]);
            }
        }

        System.out.println("-------------------------");

        for(int i= 0; i < positions.length; ++i){
            if(positions[i].equals(searchPosition) ){
                System.out.println( employeeNames[i]);
            }else{
                System.out.println("Aucun employé trouvé.");
            }
        }

        System.out.println("Hello, World!");
    }
}
